package com.goostree.codewars.thirteen;

public class Thirteen {
	private static final int[] VALUES = {1, 10, 9, 12, 3, 4};
	
	public static long thirt(long n) {
		long lastResult = 0;
		long currentResult = 0;
		
		lastResult = thirtyFi(n);
		
		while(true) {
			currentResult = thirtyFi(lastResult);
			System.out.println(lastResult + " " + currentResult);
			if(currentResult == lastResult){
				break;
			}
			
			lastResult = currentResult;
		}
		
        return currentResult;
    }
	
	//Performs the requested arbitrary calculation
	private static long thirtyFi(long x){
		long sum = 0;
		
		for(int i = 0; x > 0; i++) {
			sum += VALUES[i % 6] * (x % 10);
			x = x / 10;
		}
		
		return sum;
	}
}
